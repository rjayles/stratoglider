#ifndef GY_521_H
#define GY_521_H

#include <stdint.h>
#include <stdio.h>
#include <stdint.h>

#define GY_521_ADDRESS 0x68
#define CONFIG_REG 0x6B

/* Acceleration */
#define REG_ACCEL_X_HIGH 0x3B
#define REG_ACCEL_X_LOW 0x3C
#define REG_ACCEL_Y_HIGH 0x3D
#define REG_ACCEL_Y_LOW 0x3E
#define REG_ACCEL_Z_HIGH 0x3F
#define REG_ACCEL_Z_LOW 0x40

/* Sensitivity */
#define ACCEL_SENSITIVITY_2 16384.0
#define ACCEL_SENSITIVITY_4 8192.0
#define ACCEL_SENSITIVITY_8 4096.0
#define ACCEL_SENSITIVITY_16 2048.0

#define GYRO_SENSITIVITY_250 131.0
#define GYRO_SENSITIVITY_500 65.5
#define GYRO_SENSITIVITY_1000 32.8
#define GYRO_SENSITIVITY_2000 16.4



/* Temperature */
#define REG_TEMP_HIGH 0x41
#define REG_TEMP_LOW 0x42

/* Gyroscope */

#define REG_GYRO_X_HIGH 0x43
#define REG_GYRO_X_LOW 0x44
#define REG_GYRO_Y_HIGH 0x45
#define REG_GYRO_Y_LOW 0x46
#define REG_GYRO_Z_HIGH 0x47
#define REG_GYRO_Z_LOW 0x48

//Init the gy_521. Must be called before others actions
int init_gy_521(float sensitivity);

//Retrieve the acceleration following X axis
float get_acceleration_x();

//Retrieve the acceleration following Y axis
float get_acceleration_y();

//Retrieve the acceleration following Z axis
float get_acceleration_z();

int16_t get_gyro_x();

int16_t get_gyro_y();

int16_t get_gyro_z();

//for internal use, read a value contained in two registers
int16_t read_high_low_register(int8_t register_high, int8_t register_low);

//return the current temperature in degree
float get_temperature();

#endif
