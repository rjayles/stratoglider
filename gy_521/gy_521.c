#include <stdio.h>
#include <stdint.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include "gy_521.h"

int fd;
float sensitivity;

int init_gy_521(float sensitivity_desired){
	printf("Setup of the gy_521 sensor\n");
	//int8_t fd;
    int data;
	fd=wiringPiI2CSetup(GY_521_ADDRESS);
	sensitivity = sensitivity_desired;
    if(fd==-1){
		printf("Can't setup the I2C device for gy_521\n");
        return -1;
	}else{
		//See the manual, but we put the sleep mode at 0
		printf("Config : %x\n", wiringPiI2CReadReg8(fd, CONFIG_REG));
		wiringPiI2CWriteReg8(fd, CONFIG_REG, 0);
		printf("Setup success\n");
		return 0;
	}
}
//Retrieve the acceleration following X axis
float get_acceleration_x(){
	return read_high_low_register(REG_ACCEL_X_HIGH, REG_ACCEL_X_LOW)/sensitivity;	
}

//Retrieve the acceleration following Y axis
float get_acceleration_y(){
	return read_high_low_register(REG_ACCEL_Y_HIGH, REG_ACCEL_Y_LOW)/sensitivity;
}

//Retrieve the acceleration following Z axis
float get_acceleration_z(){
	return read_high_low_register(REG_ACCEL_Z_HIGH, REG_ACCEL_Z_LOW)/sensitivity;
}

//Retrieve the gyro following Z axis
int16_t get_gyro_x(){
	return read_high_low_register(REG_GYRO_X_HIGH, REG_GYRO_X_LOW);
}

//Retrieve the gyro following Y axis
int16_t get_gyro_y(){
	return read_high_low_register(REG_GYRO_Y_HIGH, REG_GYRO_Y_LOW);
}

//Retrieve the gyro following Z axis
int16_t get_gyro_z(){
	return read_high_low_register(REG_GYRO_Z_HIGH, REG_GYRO_Z_LOW);
}

int16_t read_high_low_register(int8_t register_high, int8_t register_low){
	int16_t value = 0;
	value = wiringPiI2CReadReg8(fd, register_high);
	value = (value << 8) | wiringPiI2CReadReg8(fd, register_low);
	return value;
}

float get_temperature(){
	int16_t temperature = read_high_low_register(REG_TEMP_HIGH, REG_TEMP_LOW);
	return temperature/340.0 + 36.53;
}
